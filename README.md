# Lucien

A minimal, cross-platform, graphical [Markdown](https://commonmark.org/) text editor

Download **Lucien.AppImage** binary from here: [Lucien.AppImage](https://gitlab.com/linuxappimage/lucien/-/jobs/artifacts/master/raw/Lucien.AppImage?job=run-build)

